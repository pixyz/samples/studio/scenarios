import threading
import time

# Runs a decimation in a thread, waits for 1 second for the viewer to refresh, and then start a new thread (and so on)
# The viewer updates on every iteration.
class MyThread(threading.Thread):
	def __init__(self, surf):
		self.surf = surf
		threading.Thread.__init__(self)

	def run(self):
		print(self.surf)
		algo.decimate([0], 10 * self.surf, 5, 10)
		time.sleep(1)
		if (self.surf < 0.5):
			m = MonThread(self.surf**0.9)
			m.start()

print("test")
m = MyThread(0.01)
m.start()