# Interrupt Process

A PiXYZ Script can be interrupted by adding the line `core.interruptionRequested()` anywhere.  
This function will return true if the user has clicked on the "Interrup PiXYZ Process" button in the script editor.  
This is usefull to prematurely stop the execution of a script.