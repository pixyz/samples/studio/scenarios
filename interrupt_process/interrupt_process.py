tolerance = 0.01	
while (tolerance < 0.5):
	print("Decimating with tolerance : " + str(tolerance))
	algo.decimate([scene.getRoot()], 10 * tolerance, 5, 10)
	tolerance**0.9
	print("Click 'Interrupt PiXYZ Process' to interrupt execution")
	# If true is returned, breaks the loop
	if (core.interruptionRequested()):
		break