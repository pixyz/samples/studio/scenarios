import sys
import os

viewerMatrices = {'views': [[[1.0, 0.0, 0.0, 0.0], [0.0, 2.220446049250313e-16, 1.0, 2.0220496850242857e-06], [0.0, -1.0, 2.220446049250313e-16, -611.442182083267], [0.0, 0.0, 0.0, 1.0]]], 'projs': [[[1.7320508075688772, 0.0, 0.0, 0.0], [0.0, 3.4252518773978915, 0.0, 0.0], [0.0, 0.0, -1.5423587825330813, -681.030696279546], [0.0, 0.0, -1.0, 0.0]]], 'clipping': pxz.geom.Point2(267.8735593726864, 1255.68298737378)}

def process_all_files_from_folder(folder, output):
    input_files = [folder + '/' + _file for _file in os.listdir(folder)\
		if (os.path.isfile(folder + '/' + _file))]
    for input_file in input_files:
      root = io.importScene(input_file)
      viewer = view.createViewer(2000, 1000)
      view.addRoot(scene.getRoot(), viewer)
      view.showLines(False, viewer)
      view.fit([root], viewer)
      #view.setViewerMatrices(viewerMatrices['views'], viewerMatrices["projs"], viewerMatrices["clipping"], viewer)
      view.refreshViewer(viewer, forceUpdate=True)
      view.takeScreenshot(os.path.basename(input_file) + '.jpg', viewer)

folder = sys.argv[1]
output = sys.argv[2]
process_all_files_from_folder(folder, output)
