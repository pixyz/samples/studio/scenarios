#--------------------------------------------------------
# Script PiXYZ STUDIO - Show View Points
#
# This Python script is meant to be used in PiXYZ STUDIO 2019
# Open the script in STUDIO Script window, and click the Execute button (CTRL+E)
# 
# This sample script can be used to rapidly identify the view points positions and directions
# from all methods using camera positions (smartHiddenRemoval, smartOrient...).
#
# Copyright PiXYZ Software - 2020
#--------------------------------------------------------

# ------ USER INPUTS --------- #

VOXEL_SIZE = 1000
MIN_CAVITY_VOLUME = 30000000
SHOW_VOXELS = False
CAMERA = 13860 # add an occurrence id (must point to up direction)
# - - - - - - - - - - - - - -  #

def createCamera(size, pos, dir, name, parent):
	occurrence = scene.createOccurrence(name)
	scene.setParent(occurrence, parent)

	sphere = scene.createSphere(size, 16, 16, False)	
	lineOccurrence = scene.createCylinder(3, 50, 16, False)
	scene.setParent(lineOccurrence, occurrence)
	scene.setParent(sphere, occurrence)
	transform = [[1.0000, 0.0000, 0.0000, 0],\
							 [0.0000, 1.0000, 0.0000, 20],\
							 [0.0000, 0.0000, 1.0000, 0],\
							 [0.0000, 0.0000, 0.0000, 1.000]]
	scene.applyTransformation(lineOccurrence, transform)
	
	assignMaterialWithRGB([lineOccurrence], [255, 0, 0, 1])
	
	rotateToDirection(occurrence, dir)
	transform = [[1.0000, 0.0000, 0.0000, pos.x],\
							 [0.0000, 1.0000, 0.0000, pos.y],\
							 [0.0000, 0.0000, 1.0000, pos.z],\
							 [0.0000, 0.0000, 0.0000, 1.000]]
	scene.applyTransformation(occurrence, transform)
	return occurrence


def rotateToDirection(occurrence, direction):
	if direction.x != 0:
		scene.rotate(occurrence, [0, 0, 1], direction.x*(-90))
	elif direction.z != 0:
		scene.rotate(occurrence, [1, 0, 0], direction.z*(90))
	elif direction.y == -1:
		scene.rotate(occurrence, [1, 0, 0], direction.y*(180))


def assignMaterialWithRGB(occurrences, rgb):
	mat = material.createMaterial(str(rgb), 'color')
	core.setProperty(mat, 'color', str(rgb))
	for oc in occurrences:
		core.setProperty(oc, 'Material', str(mat))


def instantiate(prototype, name, parent, position, direction):
	if direction.y == 1: return
	elif direction.y == -1: return
	instance = scene.prototypeSubTree(prototype)
	core.setProperty(instance, 'Name', name)
	transform = [[1.0000, 0.0000, 0.0000, position.x],\
							 [0.0000, 1.0000, 0.0000, position.y],\
							 [0.0000, 0.0000, 1.0000, position.z],\
							 [0.0000, 0.0000, 0.0000, 1.000]]

	rotateToDirection(instance, direction)
	scene.applyTransformation(instance, transform)
	core.setProperty(instance, 'Visible', 'Inherited')
	scene.setParent(instance, parent)


def showViewPoints(voxelSize, minCavityVolume, camera):
	view_points_info = scene.getViewpointsFromCavities(voxelSize, minCavityVolume)
	root_view_points = scene.createOccurrence('View Points', scene.getRoot())
	id = 0
	if camera == None:
		prototype = createCamera(voxelSize/10, Point3(0, 0, 0), Point3(0, 0, 0), 'Prototype', root_view_points)
	else:
		prototype = CAMERA
		scene.setParent(prototype, root_view_points)
	core.pushProgression(len(view_points_info['positions']))
	i = 0
	for position, direction in zip(view_points_info['positions'], view_points_info['directions']):
		core.stepProgression()
		if core.interruptionRequested():
			return root_view_points
		instantiate(prototype, 'Camera_' + str(id), root_view_points, position, direction)

		id += 1
	core.popProgression()
	scene.deleteOccurrences([prototype])
	return root_view_points


# - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
if (SHOW_VOXELS): algo.voxelize([scene.getRoot()], VOXEL_SIZE)
viewPointsRoot = showViewPoints(VOXEL_SIZE, MIN_CAVITY_VOLUME, CAMERA)
	

